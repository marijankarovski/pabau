<?php


require_once 'DatabaseConnection.php';

class User
{
    public $conn;

    public function __construct()
    {
        $db = new DatabaseConnection;
        $this->conn = $db->pdo;
    }

    public function create($data)
    {
        $sql = "INSERT INTO `users`(name, email, phone, address, blood_id) VALUES (:name, :email, :phone, :address, :blood_id)";
        $stmt = $this->conn->prepare($sql);
        $user = ['name' => $data['name'], 'email' => $data['email'], 'phone' => $data['phone'], 'address' => $data['address'], 'blood_id' => $data['blood_id']];
        $stmt->execute($user);
        $id = $this->conn->lastInsertId();
        if (isset($data['condition_id']) && count($data['condition_id']) > 0) {
            return $this->addConditions($data['condition_id'], $id);
        }
        return $id;
    }
    public function addConditions($conditions, $id)
    {
        $sql = "INSERT INTO users_conditions (user_id, condition_id) VALUES (:user_id, :conditon_id)";
        $stmt = $this->conn->prepare($sql);
        foreach ($conditions as $key => $condition) {
            $data = ['user_id' => $id, "conditon_id" => $condition];
            $stmt->execute($data);
        }
        return $id;
    }


    public function find($email)
    {
        $sql = "SELECT * FROM users WHERE email = :email";
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute(['email' => $email])) {
            $doctor = $stmt->fetch(PDO::FETCH_ASSOC);
            return $doctor;
        }
        return null;
    }
    public function findById($id)
    {

        $sql = "SELECT users.*, bloodtype.name as blood , group_concat(conditions.name) as test, group_concat(conditions.id) as cond_id FROM users
        LEFT JOIN users_conditions on users_conditions.user_id = users.id
        LEFT JOIN conditions on users_conditions.condition_id = conditions.id
        LEFT JOIN bloodtype on users.blood_id = bloodtype.id
        WHERE users.id = :id
        GROUP BY users.id";
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute(['id' => $id])) {
            $patient = $stmt->fetch(PDO::FETCH_ASSOC);
            return $patient;
        }
        return null;
    }

    public function getAll()
    {
        $sql = "SELECT users.*, bloodtype.name as blood, group_concat(conditions.name) as test FROM users
        LEFT JOIN users_conditions on users_conditions.user_id = users.id
        LEFT JOIN conditions on users_conditions.condition_id = conditions.id
        LEFT JOIN bloodtype on users.blood_id = bloodtype.id
        WHERE role = :role
        GROUP BY users.id";
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute(['role' => 'PATIENT'])) {
            $patients = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $patients;
        }
        return null;
    }

    public function delete($id)
    {

        $sql = "DELETE FROM users_conditions where user_id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['id' => $id]);

        $sql = "DELETE FROM users where id = :id";
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute(['id' => $id])) {
            return true;
        }
        return false;
    }

    public function edit($data)
    {
        $conditions = $data['condition_id'];
        $id = $data['user'];
        $sql = "UPDATE users SET name = :name, email=:email, phone = :phone, address = :address, blood_id = :blood_id WHERE id = :id";
        $stmt = $this->conn->prepare($sql);
        if ($stmt->execute(['name' => $data['name'], "email" => $data['email'], 'phone' => $data['phone'], "address" => $data['address'], "blood_id" => $data['blood_id'], 'id' => $id])) {
            $sql = "DELETE FROM users_conditions where user_id = :id";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['id' => $data['user']]);
            $this->addConditions($conditions, $id);
            return true;
        }
        return false;
    }
}
