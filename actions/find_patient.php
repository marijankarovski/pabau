<?php

require_once __DIR__ . '/../classes/User.php';
require_once __DIR__ . "/../functions.php";
onlyLoggedIn();

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header("Location: index.php");
};


$id = $_POST['id'];
$user = new User();
if ($patient = $user->findById($id)) {
    echo json_encode(['user' => $patient]);
} else {
    echo json_encode(['message' => 'error']);
}
