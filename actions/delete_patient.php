<?php

require_once __DIR__ . '/../classes/User.php';
require_once __DIR__ . "/../functions.php";
onlyLoggedIn();

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header("Location: index.php");
};

$id = $_POST['id'];
// print_r($id);
// die();
$user = new User();
if ($patients = $user->delete($id)) {
    echo json_encode(['message' => 'success']);
} else {
    echo json_encode(['message' => 'error']);
}
