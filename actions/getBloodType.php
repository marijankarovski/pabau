<?php

require_once __DIR__ . "/../classes/DatabaseConnection.php";
require_once __DIR__ . "/../functions.php";
onlyLoggedIn();


if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header("Location: index.php");
};

$db = new DatabaseConnection;
$sql = "SELECT * FROM bloodtype WHERE 1";
$stmt = $db->pdo->prepare($sql);
$stmt->execute();
$bloodType = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($bloodType);
