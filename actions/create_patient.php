<?php
require_once __DIR__ . "/../classes/User.php";
require_once __DIR__ . "/../functions.php";
onlyLoggedIn();

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header("Location: index.php");
};


if ($_POST['name'] == '' || $_POST['email'] == '' || $_POST['phone'] == '' || $_POST['address'] == '' || $_POST['blood_id'] == '') {
    echo json_encode(['message' => "empty"]);
    die();
}

$user = new User();

if ($patient = $user->create($_POST)) {
    echo json_encode(['message' => 'success']);
} else {
    echo json_encode(['message' => 'error']);
}
