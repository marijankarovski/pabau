<?php

require_once __DIR__ . '/../classes/User.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST['email'] == '' || $_POST['password'] == '') {
        echo json_encode(["message" => "empty"]);
        die();
    }
    $user = new User();
    $doctor = $user->find($_POST['email']);
    if ($doctor == null) {
        echo json_encode(["message" => "error"]);
        die();
    }
    if (md5($_POST['password']) != $doctor['password']) {
        echo json_encode(["message" => "error"]);
        die();
    }
    if ($doctor['role'] != 'DOCTOR') {
        echo json_encode(["message" => "error"]);
        die();
    }
    echo json_encode(["message" => "success"]);
    session_start();
    $_SESSION['user'] = $doctor['id'];
    die();
} else {
    echo json_encode(["message" => "error"]);
    die();
}
header("Location: index.php");
