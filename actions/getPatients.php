<?php

require_once __DIR__ . '/../classes/User.php';
require_once __DIR__ . "/../functions.php";
onlyLoggedIn();
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header("Location: index.php");
};

$user = new User();
$patients = $user->getAll();


echo json_encode($patients);
