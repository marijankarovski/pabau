<?php

require_once __DIR__ . '/../classes/User.php';
require_once __DIR__ . "/../functions.php";
onlyLoggedIn();


if ($_SERVER['REQUEST_METHOD'] != 'GET') {
    header("Location: index.php");
};

$user = new User();
if ($patients = $user->edit($_GET)) {
    echo json_encode(['message' => 'success']);
} else {
    echo json_encode(['message' => 'error']);
}
