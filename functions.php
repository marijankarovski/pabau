<?php
require_once __DIR__ . "/config.php";


function onlyLoggedIn()
{
    session_start();
    if (!isset($_SESSION['user'])) {
        header("Location: " . URL . "index.php");
        die();
    }
}
function checkifLoggedIn()
{
    session_start();
    if (isset($_SESSION['user'])) {
        header("Location: " . URL . "dashboard.php");
        die();
    }
}
