<?php

class MasterLayout
{
    public $header;
    public $footer;


    public function __construct()
    {
        $this->header = file_get_contents('layouts/header.php');
        $this->footer = file_get_contents('layouts/footer.php');
    }
}
