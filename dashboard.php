<?php
require_once __DIR__ . "/functions.php";
require_once __DIR__ . "/layouts/MasterLayout.php";
onlyLoggedIn();

$master = new MasterLayout();
echo $master->header;
?>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-8">
            <small>Add new</small> <a href="newPatient.php"><i id="addNewPatient" class="fas fa-user-plus fa-2x my-5"></i></a>
        </div>
        <div class="col-8">
            <table class="table">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="patientSearch" placeholder="name@example.com">
                    <label for="patientSearch">Search Patient</label>
                </div>
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Address</th>
                    <th>Blood Type</th>
                    <th>Medical Conditions</th>
                    <th>Actions</th>
                </thead>
                <tbody id="patients" class="text-center">

                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        getPatients();

        function getPatients() {
            $.ajax({
                type: "POST",
                url: "actions/getPatients.php",
                success: function(data) {
                    $("#patients").empty()
                    data = JSON.parse(data);
                    console.log(data);
                    data.forEach(function(value) {
                        let conditions = 'No Conditions';
                        if (value.test != null) {
                            conditions = value.test;
                        }
                        // if (value.test.length != 0) {

                        // }
                        $('#patients').append(`
                      <tr>
                      <td>${value.id}</td>
                      <td>${value.name}</td>
                      <td>${value.email}</td>
                      <td>${value.phone}</td>
                      <td>${value.address}</td>
                      <td>${value.blood}</td>
                      <td>
                      <div class="accordion accordion-flush" id="accordionExample" style="width: 200px">
                      <div class="accordion-item">
                        <h2 class="accordion-header" id="heading${value.id}">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse${value.id}" aria-expanded="false" aria-controls="collapse${value.id}">
                            Conditions
                        </button>
                        </h2>
                        <div id="collapse${value.id}" class="accordion-collapse collapse" aria-labelledby="heading${value.id}" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                        ${conditions}
                        </div>
                        </div>
                    </div>
                    </div>
                    <td>
                    <button data-id="${value.id}" id="editBtn" class="btn btn-primary">Edit</button>
                    <button data-id="${value.id}" id="deleteBtn" class="btn btn-danger">Delete</button>
                    </td>
                      </tr>
                      `)
                    })
                }
            })
        }
        $("#patientSearch").on("keyup", function() {
            var value = $(this).val();
            $("#patients tr").filter(function() {
                $(this).toggle($(this).text().indexOf(value) > -1)
            });
        });

        $(document).on('click', "#deleteBtn", function(e) {
            e.preventDefault();
            console.log($(this).attr('data-id'));
            let id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: "actions/delete_patient.php",
                data: {
                    'id': id
                },
                success: function(data) {
                    getPatients();
                }
            })
        })

        $(document).on('click', "#editBtn", function(e) {
            e.preventDefault();
            let id = $(this).attr('data-id');
            location.href = "editPatient.php?user=" + id;
        })
    })
</script>

<?php
echo $master->footer;
?>