<?php
require_once __DIR__ . "/functions.php";
require_once __DIR__ . "/layouts/MasterLayout.php";
checkifLoggedIn();
$master = new MasterLayout();
echo $master->header;
?>

<div class="container-fluid">
    <div class="row justify-content-around mt-5 pt-5">
        <div class="col-4">
            <h1>Login</h1>
            <form id="loginForm">
                <div id="error" style="visibility:hidden" class="alert alert-danger" role="alert">Wrong Email/Password Combination</div>
                <div class="form-floating mb-3">
                    <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com">
                    <label for="email">Email address</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    <label for="password">Password</label>
                </div>

                <button type="submit" id="LoginBtn" class="btn btn-primary mt-3">Login</button>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#logoutBtn').hide();
        let email = $('#email');
        let password = $('#password');
        $("#loginForm").submit(function(e) {
            e.preventDefault();
            email.css('outline', "none");
            password.css('outline', "none");
            if (email.val() == '') {
                email.css('outline', "1px solid red");
            }
            if (password.val() == '') {
                password.css('outline', "1px solid red");
            }
            $.ajax({
                type: "POST",
                url: "actions/login_doctor.php",
                data: $(this).serialize(),
                success: function(data) {
                    data = JSON.parse(data);
                    if (data.message == "success") {
                        location.href = "dashboard.php";
                        return;
                    }
                    if (data.message == "error") {
                        $('#error').css('visibility', 'visible');
                        $('#password').val('');
                        setTimeout(function() {
                            $('#error').css('visibility', 'hidden');
                        }, 2000);
                    }
                }
            })
        })
    })
</script>
<?php
echo $master->footer;
?>