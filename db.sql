CREATE TABLE users (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30) NOT NULL,
email VARCHAR(50) NOT NULL,
password VARCHAR(50),
phone VARCHAR(50) NOT NULL,
address VARCHAR(50) NOT NULL,
blood_id INT(6) UNSIGNED
);

CREATE table bloodType (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(10)
);

ALTER TABLE users 
ADD CONSTRAINT FK_users_bloodType FOREIGN KEY (blood_id) REFERENCES bloodType (id);

INSERT INTO `bloodtype`(`name`) 
VALUES ('A+'),
 ('A-'),
 ('B+'),
 ('B-'),
 ('O+'),
 ('O-'),
 ('AB+'),
 ('AB-');

CREATE table conditions (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(10)
);

INSERT INTO `conditions`(`name`) 
VALUES ('Condition1'),
 ('Condition2'),
 ('Condition3'),
 ('Condition4'),
 ('Condition5');

ALTER TABLE users
ADD COLUMN role ENUM('DOCTOR', 'PATIENT') DEFAULT 'PATIENT' AFTER blood_id;

INSERT INTO `users`(`name`, `email`, `password`, `phone`, `address`, `blood_id`, `role`) 
VALUES ("Dr. John Doe", "doctor@pabau.com", "b3666d14ca079417ba6c2a99f079b2ac", '070070070','someAddress', 2, "DOCTOR");

CREATE TABLE users_conditions (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id INT(6) UNSIGNED NOT NULL,
    condition_id INT(6) UNSIGNED NOT NULL,

    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY(condition_id) REFERENCES conditions(id)
);