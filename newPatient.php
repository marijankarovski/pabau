<?php
require_once __DIR__ . "/functions.php";
onlyLoggedIn();
require_once __DIR__ . "/layouts/MasterLayout.php";

$master = new MasterLayout();
echo $master->header;
?>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-8">
            <form id="newPatientForm">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="name" name="name" placeholder="name@example.com">
                    <label for="name">Full Name</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                    <label for="email">Email</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
                    <label for="phone">Phone Number</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Address">
                    <label for="address">Address</label>
                </div>
                <div class="mb-3 form-check" id="bloodType">
                    <p id="bloodTypeLabel">Choose blood type:</p>
                </div>
                <div class="mb-3 form-check" id="conditions">
                    <p><small>(Optional)</small> Choose known conditions:</p>
                </div>
                <button type="submit" id="LoginBtn" class="btn btn-primary mt-3">Add New Patient</button>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        let name = $("#name");
        let email = $("#email");
        let phone = $("#phone");
        let address = $("#address");
        let bloodType = $("#bloodTypeLabel");
        $.ajax({
            type: "POST",
            url: "actions/getBloodType.php",
            success: function(data) {
                data = JSON.parse(data);
                data.forEach(function(type) {
                    $("#bloodType").append(`
                    <input type="radio" name="blood_id" class="radio" id="${type.id}" value="${type.id}" style="display: none;">
                        <label class="bloodLabel text-center" for="${type.id}" style="padding: 15px; margin:5px; border:1px solid black;">${type.name}</label>
                        
                    `)
                })
            }
        })
        $.ajax({
            type: "POST",
            url: "actions/getKnownConditions.php",
            success: function(data) {
                data = JSON.parse(data);
                data.forEach(function(condition) {
                    $("#conditions").append(`
                    <input type="checkbox" name="condition_id[]" id="cond${condition.id}" value="${condition.id}" style="display: none;">
                    <label class="bloodLabel text-center" for="cond${condition.id}" style="padding: 15px; margin:5px; border:1px solid black;">${condition.name}</label>    
                    `)
                })
            }
        })
        $(document).on('submit', "#newPatientForm", function(e) {
            e.preventDefault();
            name.css("outline", "none")
            email.css("outline", "none")
            phone.css("outline", "none")
            address.css("outline", "none")
            bloodType.css("color", "black")
            if (name.val() == '') {
                name.css("outline", "1px solid red");
            }
            if (email.val() == '') {
                email.css("outline", "1px solid red");
            }
            if (phone.val() == '') {
                phone.css("outline", "1px solid red");
            }
            if (address.val() == '') {
                address.css("outline", "1px solid red");
            }
            if (!document.querySelectorAll('input[type="radio"]:checked').length) {
                bloodType.css("color", "red");
            }
            $.ajax({
                type: "POST",
                data: $(this).serialize(),
                url: "actions/create_patient.php",
                success: function(data) {
                    data = JSON.parse(data);
                    console.log(data.message);
                    if (data.message == "success") {
                        location.href = 'dashboard.php';
                    }
                }
            })
        })
    })
</script>


<?php
echo $master->footer;
?>